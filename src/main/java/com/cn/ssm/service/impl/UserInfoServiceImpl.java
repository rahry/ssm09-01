package com.cn.ssm.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cn.ssm.dao.UserInfoMapper;
import com.cn.ssm.dao.userTreeMapper;
import com.cn.ssm.domain.UserInfo;
import com.cn.ssm.domain.userTree;
import com.cn.ssm.service.IUserInfoService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import utils.PageResult;

@Service
@Transactional
public class UserInfoServiceImpl implements IUserInfoService {
	
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private userTreeMapper userTreeMapper;
	@Override
	public Map<String, Object> getUserInfolist(Map<String, String> params) {
		List<UserInfo> resultlist = new ArrayList<UserInfo>();
		Map<String, Object> resultMap = new HashMap<String, Object>();
		Map<String, Object> param = new HashMap<String, Object>();
		String id= params.get("id");
		int page = Integer.parseInt(params.get("page"));
		int rows = Integer.parseInt(params.get("rows"));
		param.put("id", id);
		param.put("page", (page-1)*rows);
		param.put("rows", rows);
		resultlist = userInfoMapper.getUserInfoList(param);
		long total = getcount(param);
		resultMap.put("rows", resultlist);
		resultMap.put("total", total);
		return resultMap;
	}
	@Override
	public void addUserInfo(UserInfo userInfo) {
		userInfoMapper.insert(userInfo);
	}
	@Override
	public long getcount(Map<String, Object> param) {
		long count = userInfoMapper.getUserInfoListCount(param);
		return count;
	}
	@Override
	public Map<String, Object> getUserInfoListByPageHelper(Map<String, String> param) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<UserInfo> resultlist = new ArrayList<UserInfo>();
		int page = Integer.parseInt(param.get("page"));
		int rows = Integer.parseInt(param.get("rows"));
		//使用pagehelper分页，必须先stratPage，然后在查询数据，顺序不可颠倒
		PageHelper.startPage(page, rows, true);
		resultlist = userInfoMapper.getUserInfoListByPageHelper(param);
		PageInfo<UserInfo> pageInfo = new PageInfo<UserInfo>(resultlist);
		resultMap.put("rows", resultlist);
		resultMap.put("total", pageInfo.getTotal());
		return resultMap;
	}
	@Override
	public PageResult<UserInfo> getUserInfoListByPageHelpers(Map<String, String> param) {
		List<UserInfo> resultlist = new ArrayList<UserInfo>();
		int page = Integer.parseInt(param.get("page"));
		int rows = Integer.parseInt(param.get("rows"));
		//使用pagehelper分页，必须先stratPage，然后在查询数据，顺序不可颠倒
		PageHelper.startPage(page, rows, true);
		resultlist = userInfoMapper.getUserInfoListByPageHelper(param);
		PageInfo<UserInfo> pageInfo = new PageInfo<UserInfo>(resultlist);
		PageResult<UserInfo> pr = new PageResult<UserInfo>();
		pr.setRows(resultlist);
		pr.setTotal(pageInfo.getTotal());
		return pr;
	}
	@Override
	public void updateUserInfo(UserInfo userinfo) {
		userInfoMapper.updateByPrimaryKey(userinfo);
	}
	@Override
	public void deleteUserInfo(String id) {
		userInfoMapper.deleteByPrimaryKey(id);
	}
	@Override
	public List<userTree> getTreeList() {
		// TODO Auto-generated method stub
		List<userTree> onelist = new ArrayList<userTree>();
		String parentId = "";
		onelist = userTreeMapper.getOnly(parentId);
		for (int i = 0; i < onelist.size(); i++) {
			//寻找孩子
			if(onelist.size() > 0){
				getChildrens(onelist.get(i),onelist);
			}
		}
		//JSONObject jsonObj = (JSONObject) JSON.toJSON(user);
		return onelist;
	}
	
	/**
	 * 寻找孩子（根据父id）
	 * @param usertree
	 * @return
	 */
	public userTree getChildrens(userTree usertree,List<userTree> onelist) {
		String newParentId = "";
		newParentId = usertree.getId();
		List<userTree> list = new ArrayList<userTree>();
		list = userTreeMapper.getOnly(newParentId);
		if(list.size() > 0 ){
			for (int i = 0; i < list.size(); i++) {
				usertree.setChildren(list);
				getChildrens(list.get(i), onelist);
			}
		}
		return usertree;
		
	}

}
