package com.cn.ssm.service;

import java.util.List;
import java.util.Map;

import com.cn.ssm.domain.UserInfo;
import com.cn.ssm.domain.userTree;
import com.github.pagehelper.PageInfo;

import utils.PageResult;


public interface IUserInfoService {
	
	public Map<String, Object> getUserInfolist(Map<String, String> param);
	
	public void addUserInfo(UserInfo userInfo);
	
	public long getcount(Map<String, Object> param);

	public Map<String, Object> getUserInfoListByPageHelper(Map<String, String> param);

	public PageResult<UserInfo> getUserInfoListByPageHelpers(Map<String, String> param);

	public void updateUserInfo(UserInfo userinfo);

	public void deleteUserInfo(String id);

	public List<userTree> getTreeList();

}
