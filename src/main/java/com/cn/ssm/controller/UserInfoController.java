package com.cn.ssm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cn.ssm.domain.UserInfo;
import com.cn.ssm.domain.userTree;
import com.cn.ssm.service.IUserInfoService;
import com.github.pagehelper.PageInfo;

import utils.PageResult;

@RestController
@RequestMapping(value = "/userinfo")
public class UserInfoController {
	 @Resource
	    private IUserInfoService userinfoService;

	    /**
	     * 手动分页
	     * @param param
	     * @return
	     */
	    @RequestMapping(value =  "/userShow" ,method = RequestMethod.GET)
	    public Map<String, Object> getUserInfoList(@RequestParam Map<String, String> param){
	    	Map<String, Object> resultMap = new HashMap<String,Object>();
	    	resultMap = userinfoService.getUserInfolist(param);
	        return resultMap;
	    }
	    
	    /**
	     * 通过mybatis 分页插件（pageHelper）查询列表
	     * @param param
	     * @return PageInfo<UserInfo>
	     */
	    @RequestMapping(value =  "/userShow/pagehelper" ,method = RequestMethod.GET)
	    public Map<String, Object> getUserInfoListBypageHelper(@RequestParam Map<String, String> param){
	    	return userinfoService.getUserInfoListByPageHelper(param);
	    }
	    
	    /**
	     * 返回一个对象
	     * @param param
	     * @return PageResult<UserInfo>
	     */
	    @RequestMapping(value =  "/userShow/pagehelpers" ,method = RequestMethod.GET)
	    public PageResult<UserInfo> getUserInfoListBypageHelperToPagerResult(@RequestParam Map<String, String> param){
	    	return userinfoService.getUserInfoListByPageHelpers(param);
	    }
	    
	    @RequestMapping(method = RequestMethod.POST)
	    public void addUserInfo(@RequestBody UserInfo userinfo){
	    	userinfoService.addUserInfo(userinfo);
	    }
	    
	    @RequestMapping(method = RequestMethod.PUT)
	    public void updateUserInfo(@RequestBody UserInfo userinfo){
	    	userinfoService.updateUserInfo(userinfo);
	    }
	    
	    @RequestMapping(value= "/{ids}", method = RequestMethod.DELETE)
	    public void deleteUserInfo(@PathVariable String ids){
	    	String []idss = ids.split(",");
	    	for (int i = 0; i < idss.length; i++) {
	    		userinfoService.deleteUserInfo(idss[i]);
			}
	    }
	    @RequestMapping(value = "/tree",method = RequestMethod.GET)
	    public List<userTree> getTreeData() {
			return userinfoService.getTreeList();
			
		}
	    
}
