/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50557
Source Host           : localhost:3306
Source Database       : ssmtest

Target Server Type    : MYSQL
Target Server Version : 50557
File Encoding         : 65001

Date: 2018-04-07 21:31:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `name` varchar(50) DEFAULT NULL COMMENT '用户姓名',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `school` varchar(200) DEFAULT NULL COMMENT '毕业院校',
  `project` varchar(255) DEFAULT NULL COMMENT '专业',
  `work` varchar(200) DEFAULT NULL COMMENT '工作',
  `company` varchar(255) DEFAULT NULL COMMENT '公司',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('01cf0c6de91d4cd680ce34b27399b30a', '张晓明', '23', '北京市安河桥旁', '123123', '123', '123123', 'sh', 'sh');
INSERT INTO `user_info` VALUES ('0d7f0d273fab40038b9c6be0c4d56618', 'zzzz', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('1', '张三', '20', '天津', '天津理工', '软件工程', '开发工程师', '梦工坊', '天津');
INSERT INTO `user_info` VALUES ('10', '李四', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('11', '李四1', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('12', '李四2', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('15', '李四5555', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('16', '李四6', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('19d3a752f2a246e0b82cf9187bc0ff7d', '1', '1', '1', '1', '1', '1', 'sh', 'tj');
INSERT INTO `user_info` VALUES ('2896842d7bfe4e19b378f27dbf3794b0', '121212', '1112', '1212', '12', '12', '11', 'sh', 'bj');
INSERT INTO `user_info` VALUES ('3764b778c0b24c4faa805479ff6904c6', '李四000', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('4', '李四9', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('4214bb056ddf4cb3ba4e298ac247a863', '55555', '555555', '55555555', '55555555555', '555555@qq.com', '555555555', '客户经理', '55555555');
INSERT INTO `user_info` VALUES ('5ee4d3251fb441489298883825196b21', '李四1111', '24', '北京1', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('5f6863f4a631431784faf5f295d44bac', '校长', '44', '00', '11', '22', '33', 'bj', 'gz');
INSERT INTO `user_info` VALUES ('6', '李四11', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('7', '李四12', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('7f89d27f3f3a416a84a87253ade48d5c', '李四1111', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('8', '李四13', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('8136defd035f4ab0a1a60128c8e0bac1', '李四00', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('9', '李四14', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('9246679ef4604c23a6586b5226c2d716', '李四0', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('9332b4bbd704470e84538ce52099753f', '李四0', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('96cfcb2b313c429ba44c085c8e75cb81', '测试001', '24', '测试001-天津', '天津理工大学', '软件工程', '软件开发', '销售主管', 'bj');
INSERT INTO `user_info` VALUES ('99cb021ca37d4f0fb43e90ae9ddc1f71', '张三001', '20', '天津', '天津理工', '软件工程', '开发工程师', '梦工坊', '天津');
INSERT INTO `user_info` VALUES ('a5985256c6c94dbfa96cbf8649625a99', '11111111111', '123123', '1111111111', '1111111111', '111111@qq.com', '123123123', '销售主管', '123123123');
INSERT INTO `user_info` VALUES ('a61d365240b34ca59743cc2057328e8f', '李四11112', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('b6a0ccdb05604593bf93d19cd79b1df9', '7777', '555555', '7777', '7777', '7777@qq.com', '7777', '系统管理员', '55555555');
INSERT INTO `user_info` VALUES ('f04eb1c2d77b4c12841b0f93a1c49a99', '李四00', '24', '北京', '北京理工', '软件工程', '开发工程师', '恒华科技', '北京');
INSERT INTO `user_info` VALUES ('fca41d13b7084282921e0ff577f3ad90', '2222222', '123123', '22222222', '2222222', '22@qq.com', '123', '系统管理员', '123123');

-- ----------------------------
-- Table structure for user_t
-- ----------------------------
DROP TABLE IF EXISTS `user_t`;
CREATE TABLE `user_t` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `age` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125556 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_t
-- ----------------------------
INSERT INTO `user_t` VALUES ('12', '123', '123', '14');
INSERT INTO `user_t` VALUES ('123', 'test', '123', '13');
INSERT INTO `user_t` VALUES ('11111', 'zahgnsan', '123', '23');
INSERT INTO `user_t` VALUES ('123456', 'username', '12id', '34');
INSERT INTO `user_t` VALUES ('125555', '123', '123', '14');

-- ----------------------------
-- Table structure for user_tree
-- ----------------------------
DROP TABLE IF EXISTS `user_tree`;
CREATE TABLE `user_tree` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父id',
  `text` varchar(100) DEFAULT NULL,
  `iconCls` varchar(50) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `checked` varchar(32) DEFAULT NULL,
  `attributes` varchar(100) DEFAULT NULL,
  `children` varchar(500) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `create_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_tree
-- ----------------------------
INSERT INTO `user_tree` VALUES ('0', '', '中国', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('1', '0', '北京', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('11', '1', '东城区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('12', '1', '西城区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('13', '1', '丰台区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('14', '1', '海淀区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('15', '1', '通州区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('16', '1', '大兴区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('2', '0', '天津', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('21', '2', '和平区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('22', '2', '河东区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('23', '2', '河西区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('24', '2', '南开区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('25', '2', '河北区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('26', '2', '滨海新区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('27', '2', '宝坻区', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('3', '0', '河北', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('4', '0', '上海', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('5', '0', '广州', null, null, null, null, null, null, null);
INSERT INTO `user_tree` VALUES ('6', '0', '深圳', null, null, null, null, null, null, null);
