<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ssm框架整合样式部分</title>
<!-- 引入 easyui -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/js/jquery-easyui-1.5.4/themes/default/easyui.css" type="text/css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/js/jquery-easyui-1.5.4/themes/icon.css" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-easyui-1.5.4/jquery.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-easyui-1.5.4/jquery.easyui.min.js" charset="UTF-8"></script>
<script src="<%=request.getContextPath()%>/js/jquery-easyui-1.5.4/locale/easyui-lang-zh_CN.js"></script>
<!-- 引入vue -->
<script src="<%=request.getContextPath()%>/js/vue.js"></script>
<!-- jQuery validate前台校验 -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.validate.min.js"></script>
</head>
<body>

</body>
</html>