<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
    href="${pageContext.request.contextPath}/jquery-easyui-1.5.4.4/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
    href="${pageContext.request.contextPath}/jquery-easyui-1.5.4.4/themes/icon.css">
<script type="text/javascript"
    src="${pageContext.request.contextPath}/jquery-easyui-1.5.4.4/jquery.min.js"></script>
<script type="text/javascript"
    src="${pageContext.request.contextPath}/jquery-easyui-1.5.4.4/jquery.easyui.min.js"></script>
<script type="text/javascript"
    src="${pageContext.request.contextPath}/jquery-easyui-1.5.4.4/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
	<h1>测试文本显示</h1>
	<ul id="tt"></ul>
</body>
<script type="text/javascript">

$(function() {  
    //使用javascript初始化  
    $('#tt').tree({  
    	method:"get",  
        url:"${pageContext.request.contextPath}/userinfo/tree",//这里写用来调用的后台的方法
        lines : true,
        animate:true,
        
        onBeforeLoad:function(){
        	//获取根节点
     　　   	var rooNode = $("#tt").tree('getRoot');
      　　    	//调用expand方法
      　　    	$("#tt").tree('expand',rooNode);
        　　},
        onLoadSuccess:function(node, data){
        	console.log('data=',data);
        	//var t = $(this);
       	 	//t.tree('expandAll'); 
        }
    });  
});  
</script>
</html>