<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
    href="${pageContext.request.contextPath}/jquery-easyui-1.5.4/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
    href="${pageContext.request.contextPath}/jquery-easyui-1.5.4/themes/icon.css">
<script type="text/javascript"
    src="${pageContext.request.contextPath}/jquery-easyui-1.5.4/jquery.min.js"></script>
<script type="text/javascript"
    src="${pageContext.request.contextPath}/jquery-easyui-1.5.4/jquery.easyui.min.js"></script>
<script type="text/javascript"
    src="${pageContext.request.contextPath}/jquery-easyui-1.5.4/locale/easyui-lang-zh_CN.js"></script>

</head>
<body>
	<div id="tb">
        <a href="javascript:openUserAddDialog()" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a> 
        <a href="javascript:openUserModifyDialog()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a> 
        <a href="javascript:deleteUser()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
        <a href="javascript:openTestTree()" class="easyui-linkbutton" iconCls="icon-add" plain="true">测试树</a> 
    <div>
    <div>
        &nbsp;用户名：&nbsp;<input type="text" id="s_userName" size="20"
            onkeydown="if(event.keyCode == 13)searchUser()" /> <a
            href="javascript:searchUser()" class="easyui-linkbutton"
            iconCls="icon-search" plain="true">查询</a>
    </div>
 	<table id="dg"></table>
 	 <div id="dlg" class="easyui-dialog"
            style="width: 730px;height:280px;padding:10px 10px;" closed="true"
            buttons="#dlg-buttons">
            <form method="post" id="fm">
                <table cellspacing="8px;">
                    <tr>
                        <td>用户名：</td>
                        <td><input type="text" id="userName" name="name"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>地址：</td>
                        <td><input type="text" id="address" name="address"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>院校：</td>
                        <td><input type="text" id="trueName" name="school"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>工作专业：</td>
                        <td><input type="text" id="email" name="project"
                            validType="email" class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>工作：</td>
                        <td><input type="text" id="phone" name="work"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>用户角色：</td>
                        <td>
                        <select name="company" class="easyui-combobox"
                            id="company" style="width: 154px;" editable="false"
                            panelHeight="auto">
                                <option value="">请选择公司</option>
                                <option value="tj">天津xxx</option>
                                <option value="bj">北京xxx</option>
                                <option value="sh">上海xxx</option>
                                <option value="sz">深圳xxx</option>
                        </select> &nbsp;<span style="color: red">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td>年龄：</td>
                        <td><input type="text" id="age" name="age"
                            class="easyui-validatebox" required="true" />&nbsp;<span
                            style="color: red">*</span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>城市：</td>
                        <td>
                        <select name="city" class="easyui-combobox"
                            id="city" style="width: 154px;" editable="false"
                            panelHeight="auto">
                                <option value="">请选择城市</option>
                                <option value="bj">北京</option>
                                <option value="tj">天津</option>
                                <option value="sh">上海</option>
                                <option value="gz">广州</option>
                        </select> &nbsp;<span style="color: red">*</span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="dlg-buttons">
            <a href="javascript:saveUser()" id="save" class="easyui-linkbutton"
                iconCls="icon-ok">保存</a>
            <a href="javascript:updateUser()" id="update" class="easyui-linkbutton"
                iconCls="icon-ok">更新</a>
            <a href="javascript:closeUserDialog()"
            class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
        </div>
        <!--easyui-tree-->
        <ul id="treeid" class="easyui-tree" checkbox="true" data-options="lines:true" style="height:94%"></ul>  
</body>
<script type="text/javascript">
		var url;
 		var basepath = '<%=request.getContextPath()%>';
 		console.log('basepath=',basepath);
 		$(function() {
//			var paramObj = {
//		    	pageSize : 10,
//		   		params : ''
//			};
// 			paramObj.queryParameters['page']= 1;
//			paramObj.queryParameters['rows']= paramObj.pageSize;
 			$('#dg').datagrid({
 			    //url:basepath + '/userRest/topage',
	    		//url:basepath + '/userinfo/userShow',
 			    url:basepath + '/userinfo/userShow/pagehelpers',
 			    method:'get',
// 			    queryParams:paramObj.queryParameters,
 			    singleSelect:true,
 			    rownumbers:true,
 			    pagination:true,
 			    pagePosition:'bottom',
 			    pageNumber:1,
 			    pageSize:10,
 			    pageList:[10,20,30,40,50],
 			    autoRowHeight:false,
 			    fitColumns:true,
 			    columns:[[
// 			    	{field:'id',title:'编号',width:100,align:'center'},
 					{field:'name',title:'用户名',width:100,align:'center'},
 					{field:'address',title:'地址',width:100,align:'center'},
 					{field:'age',title:'年龄',width:100,align:'center'},
 					{field:'school',title:'院校',width:100,align:'center'},
 					{field:'work',title:'工作',width:100,align:'center'},
 					{field:'company',title:'公司',width:100,align:'center'},
 					{field:'city',title:'城市',width:100,align:'center'}
 			    ]],
 			    onLoadSuccess:function(data){
 			    	console.log('data===',data)
 			    }
 			});
 		});
 		//打开树页面
 		function openTestTree(){
 			$('#update').hide();
 			var URL = "";
 			URL = "${pageContext.request.contextPath}/tree_demo";
 			window.open(URL)
 		}
 		//打开添加框
 		function openUserAddDialog() {
 			$('#update').hide();
	        $("#dlg").dialog("open").dialog("setTitle", "添加用户信息");
	        url = "${pageContext.request.contextPath}/userinfo";
	    }
 		//打开编辑框
 		function openUserModifyDialog() {
 			$('#save').hide();
	        var selectedRows = $("#dg").datagrid("getSelections");
	        if (selectedRows.length != 1) {
	            $.messager.alert("系统提示", "请选择一条要编辑的数据！");
	            return;
	        }
	        var row = selectedRows[0];
	        $("#dlg").dialog("open").dialog("setTitle", "编辑用户信息");
	        $("#fm").form("load", row);
	    }
 		//保存功能
 		function saveUser() {
 			var jsonData = $("#fm").serializeArray();
 			console.log('jsonData=',jsonData)
 			var values = {};  
	        for( x in jsonData ){  
	           values[jsonData[x].name] = jsonData[x].value;  
	        }  
	         var data = JSON.stringify(values) 
	         console.log('data=',data)
			 $.ajax({
		    	type:"post",
		    	url:url,
		    	data:data,
		    	async:true,
		    	contentType: 'application/json',
		    	success:function(data){
		    		console.log("data==",data)
		    		$.messager.alert("系统提示", "保存成功！");
                    resetValue();
                    $("#dlg").dialog("close");
                    $("#dg").datagrid("reload");
		    	},
		    	error:function(){
		    		console.log("data==",12222222)
		    		$.messager.alert("系统提示", "保存失败！");
                    return;
		    	},
		    });
    	}
 		//更新功能
 		function updateUser() {
	        var jsonData = $("#fm").serializeArray();
   			console.log('jsonData=',jsonData)
   			var values = {};  
	        for( x in jsonData ){  
	           values[jsonData[x].name] = jsonData[x].value;  
	        }
			 var row = $("#dg").datagrid("getSelected");
	         var data1 = JSON.stringify(row) 
	         values["id"] = row.id;
	         var data = JSON.stringify(values)
	        url = "${pageContext.request.contextPath}/userinfo";
	        $.ajax({
		    	type:"put",
		    	url:url,
		    	data:data,
		    	async:true,
		    	contentType: 'application/json',
		    	success:function(data){
		    		$.messager.alert("系统提示", "更新成功！");
                    resetValue();
                    $("#dlg").dialog("close");
                    $("#dg").datagrid("reload");
		    	},
		    	error:function(){
		    		$.messager.alert("系统提示", "更新失败！");
                    return;
		    	},
		    });
	    }
 		//删除功能
 		function deleteUser() {
        var selectedRows = $("#dg").datagrid("getSelections");
        if (selectedRows.length == 0) {
            $.messager.alert("系统提示", "请选择要删除的数据！");
            return;
        }
        var strIds = [];
        for ( var i = 0; i < selectedRows.length; i++) {
            strIds.push(selectedRows[i].id);
        }
        var ids = strIds.join(",");
        url = "${pageContext.request.contextPath}/userinfo/"+ids;
        $.messager.confirm("系统提示", "您确定要删除这<font color=red>"
                + selectedRows.length + "</font>条数据吗？", function(r) {
            if (r) {
				 $.ajax({
			    	type:"delete",
			    	url:url,
			    	//data:data,
			    	async:true,
			    	contentType: 'application/json',
			    	success:function(data){
			    		$.messager.alert("系统提示", "删除成功！");
	                    resetValue();
	                    $("#dg").datagrid("reload");
			    	},
			    	error:function(){
			    		$.messager.alert("系统提示", "删除失败！");
	                    return;
			    	},
			    });
            }
        });
    }
 	//关闭弹框	
	function closeUserDialog() {
	    $("#dlg").dialog("close");
	    resetValue();
    }
	//清空数据
	function resetValue() {
	    $("#userName").val("");
	    $("#password").val("");
	    $("#trueName").val("");
	    $("#email").val("");
	    $("#phone").val("");
	    $("#roleName").combobox("setValue", "");
    }
	//查询功能
	function searchUser() {
        $("#dg").datagrid('load', {
            "name" : $("#s_userName").val()
        });
    }
 		
 	</script>
</html>